/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gg;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import javax.swing.JLabel;

/**
 *
 * @author Isadora
 */
public class FXMLDocumentController implements Initializable {
    
    @FXML
    private Label label;
    
        @FXML
    private TextField TxtUser;
        @FXML
    private TextField TxtPassword;
        @FXML
    private Label Aviso;
       
    
        @FXML
    private void Login(ActionEvent event) throws Exception{
        
        if(TxtUser.getText().equals("test") && TxtPassword.getText().equals("test")){
            ((Node)(event.getSource())).getScene().getWindow().hide();
            Parent parent = FXMLLoader.load(getClass().getResource("/gg/Programa.fxml"));
            Stage stage = new Stage();
            Scene scene = new Scene(parent);
            stage.setScene(scene);
            stage.show();
        }else{    
            Aviso.setText("user or password invalid");
            
        }
            
    }
    
    private void handleClose(ActionEvent event) {
        System.exit(0);
    }
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    
    
}
