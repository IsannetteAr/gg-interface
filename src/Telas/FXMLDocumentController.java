/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Telas;

import Modelo.Usuário;
import Visao.Teste_DAO_Usuario;
import dataAccess.FabricaDeConexoes;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import javax.swing.JLabel;

/**
 *
 * @author Isadora
 */
public class FXMLDocumentController implements Initializable {

    @FXML
    private Label label;

    @FXML
    private TextField TxtUser;
    @FXML
    private TextField TxtPassword;
    @FXML
    private Label Aviso;

    @FXML
    private void Login(ActionEvent event) throws Exception {
        FabricaDeConexoes C = new FabricaDeConexoes();
        Usuário aux = new Usuário();
        String nomeUser = TxtUser.getText();
        String senha = TxtPassword.getText();
        if (aux.Login(nomeUser, senha)) {
            ((Node) (event.getSource())).getScene().getWindow().hide();
            System.out.println("Você está logado");
            int id = aux.getIDNome(nomeUser);
            boolean admin = aux.getADM(id);
            System.out.println(id + "- " + admin);
                Parent parent = FXMLLoader.load(getClass().getResource("/Telas/TelaDeCarregamento.fxml"));
                Stage stage = new Stage();
                Scene scene = new Scene(parent);
                stage.setScene(scene);
                stage.show();
                if (admin) {
                    Teste_DAO_Usuario Janela = new Teste_DAO_Usuario();
                    Janela.setVisible(true);
                }
            } else {
                System.out.println("Você não está logado");
                Aviso.setText("user or password invalid");

            }

        }
    
    

    private void handleClose(ActionEvent event) {
        System.exit(0);
    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }

}
